#!/usr/bin/python3

import sqlite3
import pandas as pd

# General settings
database = "crawler.db"

con = sqlite3.connect(database)
print(pd.read_sql_query("SELECT * FROM webpage", con))
print(pd.read_sql_query("SELECT * FROM queue", con))
con.close()
