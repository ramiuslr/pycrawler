#!/usr/bin/python3

import requests
import sqlite3
from bs4 import BeautifulSoup
from datetime import datetime
from urllib.parse import urlparse


# General settings
entrypoint = ["https://www.google.com"]
database = "crawler.db"
f_deep = False # Scan tld only if set to False, eg example.com, not example.com/a-page


# Open log file
log = open("crawler.log", "a", buffering=1)


class WebPage:
    def __init__(self, url, title, keywords, hrefs, scan_date):
        self.url = url
        self.title = title
        self.keywords = keywords
        self.hrefs = hrefs
        self.scan_date = scan_date


def init_db():
    # Create a database, then a table if those doesn't exist
    con = sqlite3.connect(database)
    cur = con.cursor()
    cur.execute("CREATE TABLE webpage(url, title, keywords, hrefs, scan_date)")
    cur.execute("CREATE TABLE queue(url)")
    con.close()


def pop_from_queue():
    # Open db connection
    con = sqlite3.connect(database)
    con.row_factory = lambda cursor, row: row[0]
    cur = con.cursor()

    # Register the first line value into memory then remove it from db
    url = cur.execute("SELECT url FROM queue").fetchone()
    cur.execute("DELETE FROM queue WHERE url ='" + url + "'")

    # Remove it from database
    con.commit()
    con.close()

    return url


def append_to_queue(urls):
    # Open db connection
    con = sqlite3.connect(database)
    cur = con.cursor()

    # Build dataset to send to db
    data = []
    for url in urls:
        url  = [url]
        data.append(tuple(url))

    # Append to db, then close the connection
    cur.executemany("INSERT INTO queue VALUES(?)", data)
    con.commit()
    con.close()


def is_in_db(url):
    # Request all urls into database
    con = sqlite3.connect(database)
    con.row_factory = lambda cursor, row: row[0]
    cur = con.cursor()
    urls = cur.execute("SELECT url FROM webpage").fetchall()
    con.close()

    # Handle empty database
    if len(urls) == 0:
        return False

    # Check if new url was already scanned
    if urls.count(url) == 0:
        return False
    else:
        return True


def append_to_db(w):
    # Open db connection
    con = sqlite3.connect(database)
    cur = con.cursor()

    # Build the dataset for the query
    data = [(w.url, w.title, ' '.join(w.keywords), ' '.join(w.hrefs), w.scan_date)]
    
    # Append to db, then close the connection
    cur.executemany("INSERT INTO webpage VALUES(?, ?, ?, ?, ?)", data)
    con.commit()
    con.close()

#
# --- END OF FUNCTIONS DECLARATIONS ---
#

# Try to init db (will fail and skip if the table already exists)
try:
    init_db()
    # Init queue with entrypoint
    append_to_queue(entrypoint)
except:
    log.write("Database already initialized, skipping\n")

# Make request until we manually break it
while KeyboardInterrupt:
    # Pop our next url to scan
    url = pop_from_queue()

    # Check that we didn't scan the url before
    if is_in_db(url):
        log.write("Skipping url in db --> " + url + "\n")
    else:
        log.write("Scanning --> " + url + "\n")

        # Make the request
        try:
            r = requests.get(url)
        except:
            log.write("Failed to get " + url + " -- code " + str(r.status_code) + ", skipping\n")
            continue

        # Page parsing itself
        soup = BeautifulSoup(r.text, 'html.parser')

        # Title
        try:
            title = soup.find('title').text
        except:
            title = "No title found"
    
        # Links
        hrefs = []
        for link in soup.find_all('a'):
            href = link.get('href')

            # Parse url
            parsed_url = urlparse(href)

            # Skip invalid urls
            if len(parsed_url.netloc) < 1:
                continue

            # Build clean url
            if f_deep == True:
                clean_url = "https://" + parsed_url.netloc + parsed_url.path
            else:
                clean_url = "https://" + parsed_url.netloc
            
            hrefs.append(clean_url)

        # Create our webpage object
        w = WebPage(url, title, ["some", "keywords"], [], datetime.now().timestamp())

        # Append found urls to our webpage object, and to queue
        for href in hrefs:
            w.hrefs.append(href)

        append_to_queue(hrefs)

        # Append scanned webpage to database
        append_to_db(w)

log.close()
